from hydra import compose, initialize

DATALOAD = ["data", "target"]
DATASETS = ["train", "val"]

initialize(version_base=None, config_path='src/snake_task/conf')
hydra_cfg = compose(config_name="config")

rule train:
    input:
        expand("data/{dataset}_{dataload}.csv", dataset=DATASETS, dataload=DATALOAD)
    output:
        "src/snake_task/models/model.pkl"
    threads: hydra_cfg['cores'] / 2
    params:
        C = hydra_cfg['model']['params']['C']
    script:
        "src/snake_task/scripts/model_train.py"

rule data_preprocess:
    input:
        expand("data/{dataload}.csv", dataload=DATALOAD)
    output:
        expand("data/{dataset}_{dataload}.csv", dataset=DATASETS, dataload=DATALOAD)
    params:
        test_size = hydra_cfg['dataset']['test_size'],
        random_state = hydra_cfg['dataset']['random_state']
    script:
        "src/snake_task/scripts/data_preprocessing.py"

rule data_load:
    output:
        expand("data/{dataload}.csv", dataload=DATALOAD)
    script:
        "src/snake_task/scripts/data_load.py"




