import numpy as np
from sklearn.linear_model import LogisticRegression
import pickle

X_train = np.genfromtxt(snakemake.input[0], delimiter=",")
X_test = np.genfromtxt(snakemake.input[2], delimiter=",")

y_train = np.genfromtxt(snakemake.input[1], delimiter=",")
y_test = np.genfromtxt(snakemake.input[3], delimiter=",")


# Create an instance of Logistic Regression Classifier and fit the data.
logreg = LogisticRegression(C=snakemake.params.C)
logreg.fit(X_train, y_train)

pickle.dump(logreg, open(snakemake.output[0], 'wb'))
