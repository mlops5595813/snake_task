import os
import numpy as np
from sklearn import datasets

iris = datasets.load_iris()

X = iris.data
Y = iris.target

cwd_path = os.getcwd()

np.savetxt(snakemake.output[0], X, delimiter=",")
np.savetxt(snakemake.output[1], Y, delimiter=",")
