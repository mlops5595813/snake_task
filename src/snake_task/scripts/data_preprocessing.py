from sklearn.model_selection import train_test_split
import numpy as np

X = np.genfromtxt(snakemake.input[0], delimiter=",")
Y = np.genfromtxt(snakemake.input[1], delimiter=",")

X_train, X_test, y_train, y_test = train_test_split(
    X, Y, test_size=snakemake.params.test_size, random_state=snakemake.params.random_state
)

np.savetxt(snakemake.output[0], X_train, delimiter=",")
np.savetxt(snakemake.output[2], X_test, delimiter=",")

np.savetxt(snakemake.output[1], y_train, delimiter=",")
np.savetxt(snakemake.output[3], y_test, delimiter=",")
